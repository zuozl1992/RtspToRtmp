#include <QFile>
#include <QMutex>
#include <QSettings>
#include <QVariant>
#include "configure.h"
#define SETFILE         "urlcfg.ini"
#define URLGROUP        "URL"
#define RTSPURL         "RTSP"
#define RTMPURL         "RTMP"
#define POSTURL         "POST"
#define GETURL          "GET"
#define GETURL2         "GET2"
#define HLSBOTH         "HlsBoth"
#define HLSPORT         "HlsPort"
#define HLSGROUP        "HlsGroup"
#define HLSHTTPS        "HlsHttps"
#define RTSPCONTYPE     "RtspConnectType"
#define POSTID          "PostID"
#define GETKEY          "GetKey"
#define GETKEY2         "GetKey2"

Configure::Configure(QObject *parent) : QObject(parent)
{
    initSettingFile();
}

QString Configure::getRtsp()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(RTSPURL).toString();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getRtmp()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(RTMPURL).toString();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getPostUrl()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(POSTURL).toString();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getGetUrl()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(GETURL).toString();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getGet2Url()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(GETURL2).toString();
    set->endGroup();
    delete set;
    return re;
}

bool Configure::getHlsBoth()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    bool re = set->value(HLSBOTH).toBool();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getHlsGroup()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(HLSGROUP).toString();
    set->endGroup();
    delete set;
    return re;
}

int Configure::getHlsPort()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    int re = set->value(HLSPORT).toInt();
    set->endGroup();
    delete set;
    return re;
}

bool Configure::getHlsHttps()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    bool re = set->value(HLSHTTPS).toBool();
    set->endGroup();
    delete set;
    return re;
}

bool Configure::getRtspConnectType()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    bool re = set->value(RTSPCONTYPE).toBool();
    set->endGroup();
    delete set;
    return re;
}

int Configure::getPostID()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    int re = set->value(POSTID).toInt();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getGetKey()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(GETKEY).toString();
    set->endGroup();
    delete set;
    return re;
}

QString Configure::getGet2Key()
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QString re = set->value(GETKEY2).toString();
    set->endGroup();
    delete set;
    return re;
}

void Configure::setRtsp(QString url)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(RTSPURL, url);
    set->endGroup();
    delete set;
}

void Configure::setRtmp(QString url)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(RTMPURL, url);
    set->endGroup();
    delete set;
}

void Configure::setPostUrl(QString url)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(POSTURL, url);
    set->endGroup();
    delete set;
}

void Configure::setGetUrl(QString url)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(GETURL, url);
    set->endGroup();
    delete set;
}

void Configure::setGet2Url(QString url)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(GETURL2, url);
    set->endGroup();
    delete set;
}

void Configure::setHlsBoth(bool ok)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(HLSBOTH, ok);
    set->endGroup();
    delete set;
}

void Configure::setHlsGroup(QString group)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(HLSGROUP, group);
    set->endGroup();
    delete set;
}

void Configure::setHlsPort(int port)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(HLSPORT, port);
    set->endGroup();
    delete set;
}

void Configure::setHlsHttps(bool ok)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(HLSHTTPS, ok);
    set->endGroup();
    delete set;
}

void Configure::setRtspConnectType(bool type)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(RTSPCONTYPE, type);
    set->endGroup();
    delete set;
}

void Configure::setPostID(int id)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(POSTID, id);
    set->endGroup();
    delete set;
}

void Configure::setGetKey(QString key)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(GETKEY, key);
    set->endGroup();
    delete set;
}

void Configure::setGet2Key(QString key)
{
    static QMutex prMutex;
    QMutexLocker locker(&prMutex);
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    set->setValue(GETKEY2, key);
    set->endGroup();
    delete set;
}

Configure *Configure::getObject()
{
    static Configure cf;
    return &cf;
}

void Configure::initSettingFile()
{
    QSettings *set = new QSettings(SETFILE,QSettings::IniFormat);
    set->beginGroup(URLGROUP);
    QStringList list = set->childKeys();
    if(!list.contains(RTSPURL))
        set->setValue(RTSPURL,"");
    if(!list.contains(RTMPURL))
        set->setValue(RTMPURL,"");
    if(!list.contains(POSTURL))
        set->setValue(POSTURL,"");
    if(!list.contains(GETURL))
        set->setValue(GETURL,"");
    if(!list.contains(GETURL2))
        set->setValue(GETURL2,"");
    if(!list.contains(HLSBOTH))
        set->setValue(HLSBOTH,true);
    if(!list.contains(HLSGROUP))
        set->setValue(HLSGROUP,"hls");
    if(!list.contains(HLSPORT))
        set->setValue(HLSPORT,8080);
    if(!list.contains(HLSHTTPS))
        set->setValue(HLSHTTPS,false);
    if(!list.contains(POSTID))
        set->setValue(POSTID,0);
    if(!list.contains(GETKEY))
        set->setValue(GETKEY,"");
    if(!list.contains(GETKEY2))
        set->setValue(GETKEY2,"");
    if(!list.contains(RTSPCONTYPE))
        set->setValue(RTSPCONTYPE,true);
    set->endGroup();
    delete set;
}
