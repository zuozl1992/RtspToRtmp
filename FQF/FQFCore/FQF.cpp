#include "FQF.h"
extern "C"
{
	#include <libavformat/avformat.h>
    #include <libavdevice/avdevice.h>
}

FQF::FQF()
{
	av_register_all();
	avformat_network_init();
    avdevice_register_all();
}


FQF::~FQF()
{
}
