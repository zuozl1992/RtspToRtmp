#include "ThrmbnailWidget.h"
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QAction>
#include <QDebug>
ThrmbnailWidget::ThrmbnailWidget(QWidget *parent)
	: QWidget(parent)
{
	thrmbnail = new FQFMediaThumbnail;
	name = new QLabel;
	this->setFixedSize(240, 190);
	thrmbnail->setFixedSize(240,135);
	name->setFixedSize(240, 30);
	name->setAlignment(Qt::AlignCenter);
	name->setStyleSheet("color:white;font:18pt '黑体'");
	QVBoxLayout *v = new QVBoxLayout;
	v->addWidget(thrmbnail);
	v->addWidget(name);
	this->setLayout(v);
	QAction *del = new QAction("&删除");
    this->setStyleSheet("color:white");
	connect(del, SIGNAL(triggered(bool)),
		this, SLOT(delTriggered(bool)));
	addAction(del);
	setContextMenuPolicy(Qt::ActionsContextMenu);
    connect(thrmbnail,SIGNAL(doubleClicked()),this,SLOT(receiveFQFMediaThumSlot()));
}


ThrmbnailWidget::~ThrmbnailWidget()
{
}

void ThrmbnailWidget::setThumInfo(QString id,QString admin, QString passwd, QString addr, QString port, QString fps)
{
    this->id = id;
	this->admin = admin;
	this->passwd = passwd;
	this->addr = addr;
	this->port = port;
	this->fps = fps;
    QString url = QString("rtsp://%1:%2@%3:%4").arg(admin,passwd,addr,port);
    thrmbnail->openStream(url.toStdString().c_str());
}

void ThrmbnailWidget::setName(QString name)
{
	this->name->setText(name);
}

void ThrmbnailWidget::delTriggered(bool)
{
    emit deleteVideo(id);
}

void ThrmbnailWidget::receiveFQFMediaThumSlot()
{
    emit  sendVideoWidget(admin,passwd,addr,port,fps);
}

