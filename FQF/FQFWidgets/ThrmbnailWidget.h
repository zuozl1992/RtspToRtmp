#pragma once
#include <qwidget.h>
#include <qlabel.h>
#include "FQFMediaThumbnail.h"
class ThrmbnailWidget :
	public QWidget
{
	Q_OBJECT
public:
	ThrmbnailWidget(QWidget *parent = Q_NULLPTR);
	virtual ~ThrmbnailWidget();
	//打开媒体获取图片，如需刷新可以多次调用
	//@parma url rtsp地址
	//@return bool 操作结果

    void setThumInfo(QString id, QString admin, QString passwd, QString addr, QString port, QString fps);

	void setName(QString name);

public slots:
	void delTriggered(bool);
    void receiveFQFMediaThumSlot();

signals:
    void sendVideoWidget(QString admin,QString passwd,QString addr,QString port,QString fps);

    void deleteVideo(QString id);

private:

	FQFMediaThumbnail *thrmbnail;
	QLabel *name;
	//
    QString id;
	QString addr;
	QString port;
    QString admin;
	QString passwd;
	QString fps;
};

