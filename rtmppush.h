#ifndef RTMPPUSH_H
#define RTMPPUSH_H

#include <QWidget>
#include <QTimer>
#include "FQF/FQFPush/FQFPush.h"
namespace Ui {
class RtmpPush;
}

class RtmpPush : public QWidget
{
    Q_OBJECT

public:
    explicit RtmpPush(QWidget *parent = 0);
    ~RtmpPush();

private slots:
    void on_push_clicked();
    void timeoutSlot();
    void timeoutSlot2();
    void on_stop_clicked();

    void on_btnSubmit_clicked();

    void on_cbConnectType_currentIndexChanged(int index);

    void on_btnSubmit_2_clicked();

    void on_hlsBoth_clicked(bool checked);

    void on_btnSubmit_3_clicked();

private:
    void prstop();
    void prstart();

    Ui::RtmpPush *ui;
    FQFPush *push;
    QTimer *timer,*timer2;
    bool stat = false;
    int errornum = 0;
};

#endif // RTMPPUSH_H
