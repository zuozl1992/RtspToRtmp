﻿#include "rtmppush.h"
#include "ui_rtmppush.h"
#include <QMessageBox>
#include "httprequest.h"
#include "configure.h"

RtmpPush::RtmpPush(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RtmpPush)
{
    ui->setupUi(this);
    push = new FQFPush;
    timer = new QTimer;
    timer2 = new QTimer;
    ui->rtsp->setText(Configure::getObject()->getRtsp());
    ui->rtmp->setText(Configure::getObject()->getRtmp());
    ui->leHttpPostUrl->setText(Configure::getObject()->getPostUrl());
    ui->HlsGroup->setText(Configure::getObject()->getHlsGroup());
    ui->HlsPort->setText(QString("%1").arg(Configure::getObject()->getHlsPort()));
    ui->leHttpGetUrl->setText(Configure::getObject()->getGetUrl());
    ui->leHttpGetUrl_2->setText(Configure::getObject()->getGet2Url());
    ui->id->setText(QString("%1").arg(Configure::getObject()->getPostID()));
    ui->key->setText(Configure::getObject()->getGetKey());
    ui->key_2->setText(Configure::getObject()->getGet2Key());
    ui->isHttps->setChecked(Configure::getObject()->getHlsHttps());
    ui->hlsBoth->setChecked(Configure::getObject()->getHlsBoth());
    on_hlsBoth_clicked(ui->hlsBoth->isChecked());
    bool t = Configure::getObject()->getRtspConnectType();
    if(t)
        ui->cbConnectType->setCurrentIndex(1);
    else
        ui->cbConnectType->setCurrentIndex(0);
    ui->stop->setEnabled(false);
    connect(timer,SIGNAL(timeout()),
            this,SLOT(timeoutSlot()));
    connect(timer2,SIGNAL(timeout()),
            this,SLOT(timeoutSlot2()));
    this->setWindowTitle("RTSP to RTMP V2.0");
    this->setWindowIcon(QIcon(":/logo.ico"));
}

RtmpPush::~RtmpPush()
{
    delete ui;
}

void RtmpPush::on_push_clicked()
{
    switch (ui->cbConnectType->currentIndex()) {
    case 0:
        Configure::getObject()->setRtspConnectType(false);
        push->setRtspConnectType(false);
        break;
    case 1:
        Configure::getObject()->setRtspConnectType(true);
        push->setRtspConnectType(true);
        break;
    }
    push->startPush(ui->rtsp->text().toStdString().c_str(),
                    ui->rtmp->text().toStdString().c_str());
    timer->start(1000);
    timer2->start(10000);
    ui->push->setEnabled(false);
    ui->rtsp->setEnabled(false);
    ui->rtmp->setEnabled(false);
    ui->hlsBoth->setEnabled(false);
    ui->HlsGroup->setEnabled(false);
    ui->HlsPort->setEnabled(false);
    ui->isHttps->setEnabled(false);
    ui->cbConnectType->setEnabled(false);
    ui->stop->setEnabled(true);
    ui->relab->setText(QString::fromLocal8Bit("正在尝试推流，请等待结果。。。"));
}

void RtmpPush::timeoutSlot()
{
    bool temp = push->getState();
    if(!temp)
    {
        errornum++;
        if(errornum >= 11)
        {
            prstop();
            prstart();
        }
        if(errornum > 100)
            errornum = 100;
    }
    else {
        errornum = 0;
    }
    if(temp != stat)
    {
        stat = temp;
        if(temp)
        {
            Configure::getObject()->setRtsp(ui->rtsp->text());
            Configure::getObject()->setRtmp(ui->rtmp->text());
            Configure::getObject()->setPostUrl(ui->leHttpPostUrl->text());
            Configure::getObject()->setHlsBoth(ui->hlsBoth->isChecked());
            if(ui->hlsBoth->isChecked()){
                Configure::getObject()->setHlsGroup(ui->HlsGroup->text());
                Configure::getObject()->setHlsPort(ui->HlsPort->text().toInt());
                Configure::getObject()->setHlsHttps(ui->isHttps->isChecked());
            }
            Configure::getObject()->setGetUrl(ui->leHttpGetUrl->text());
            Configure::getObject()->setGetKey(ui->key->text());
            Configure::getObject()->setGet2Url(ui->leHttpGetUrl_2->text());
            Configure::getObject()->setGet2Key(ui->key_2->text());
            Configure::getObject()->setRtspConnectType(ui->cbConnectType->currentIndex() == 0 ? false : true);
            ui->relab->setText(QString::fromLocal8Bit("推流成功，正在推流。。。"));
            ui->textBrowser->clear();
            ui->textBrowser->append(QString::fromLocal8Bit("Stream 地址：%1").arg(push->getRtmpKey()));
            ui->textBrowser->append(" ");
            ui->textBrowser->append(QString::fromLocal8Bit("RTMP 地址："));
            ui->textBrowser->append(push->getPushUrl());
            QString str;
            str.clear();
            if(ui->hlsBoth->isChecked()){
                str = (ui->isHttps->isChecked() ? "https://" : "http://") + push->getRtmpIp() + ":" + ui->HlsPort->text() +
                        "/" + ui->HlsGroup->text() + "/" + push->getRtmpKey() + ".m3u8";
                ui->textBrowser->append(" ");
                ui->textBrowser->append(QString::fromLocal8Bit("hls播放地址："));
                ui->textBrowser->append(str);
            }
            else
                str = "http://empty.com";
            ui->textBrowser->append(" ");
            if(!ui->leHttpPostUrl->text().isEmpty())
            {
                bool ok = HttpRequest::getObject()->uploadHlsUrl(ui->leHttpPostUrl->text(),
                                                       push->getPushUrl(),
                                                       str,ui->id->text().toInt());
                if(ok){
                    if(ui->hlsBoth->isChecked()){
                        ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址POST提交成功"));
                    }
                    else
                    {
                        ui->textBrowser->append(QString::fromLocal8Bit("新RTMP/HLS地址POST提交成功"));
                    }
                    Configure::getObject()->setPostUrl(ui->leHttpPostUrl->text());
                    Configure::getObject()->setPostID(ui->id->text().toInt());
                }
                else
                    ui->textBrowser->append(QString::fromLocal8Bit("新地址POST提交失败，请检查地址并点击提交按钮"));
            }
            else
            {
                ui->textBrowser->append(QString::fromLocal8Bit("POST提交地址为空，放弃提交，可手动提交"));
            }
            ui->textBrowser->append(" ");
            if(!ui->leHttpGetUrl->text().isEmpty() && ui->hlsBoth->isChecked())
            {
                bool ok = HttpRequest::getObject()->uploadHlsUrl2(ui->leHttpGetUrl->text(),
                                                       str, ui->key->text());
                if(ok){
                    ui->textBrowser->append(QString::fromLocal8Bit("新HLS地址GET提交成功"));
                    Configure::getObject()->setGetUrl(ui->leHttpGetUrl->text());
                    Configure::getObject()->setGetKey(ui->key->text());
                }
                else
                    ui->textBrowser->append(QString::fromLocal8Bit("新HLS地址GET提交失败，请检查地址并点击提交按钮"));
            }
            else
            {
                ui->textBrowser->append(QString::fromLocal8Bit("HLS GET提交地址为空或未选择HLS功能，放弃提交，可手动提交"));
            }
            ui->textBrowser->append(" ");
            if(!ui->leHttpGetUrl_2->text().isEmpty())
            {
                bool ok = HttpRequest::getObject()->uploadHlsUrl2(ui->leHttpGetUrl_2->text(),
                                                       push->getPushUrl(), ui->key_2->text());
                if(ok){
                    ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址GET提交成功"));
                    Configure::getObject()->setGet2Url(ui->leHttpGetUrl_2->text());
                    Configure::getObject()->setGet2Key(ui->key_2->text());
                }
                else
                    ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址GET提交失败，请检查地址并点击提交按钮"));
            }
            else
            {
                ui->textBrowser->append(QString::fromLocal8Bit("RTMP GET提交地址为空，放弃提交，可手动提交"));
            }
        }
        else
        {
            ui->relab->setText(QString::fromLocal8Bit("推流失败，正在自动重试。。。"));
        }
    }
}

void RtmpPush::timeoutSlot2()
{
    timer2->stop();
    if(!push->getState())
    {
        on_stop_clicked();
        QMessageBox::warning(this,"warning",QString::fromLocal8Bit("推流失败，请检查配置"));
    }
}

void RtmpPush::on_stop_clicked()
{
    push->stopPush();
    ui->push->setEnabled(true);
    ui->rtsp->setEnabled(true);
    ui->rtmp->setEnabled(true);
    ui->cbConnectType->setEnabled(true);
    ui->hlsBoth->setEnabled(true);
    if(ui->hlsBoth->isChecked()){
        ui->HlsGroup->setEnabled(true);
        ui->HlsPort->setEnabled(true);
        ui->isHttps->setEnabled(true);
    }
    ui->stop->setEnabled(false);
    timer->stop();
    timer2->stop();
    stat = false;
    ui->relab->setText(QString::fromLocal8Bit("推流停止。。。"));
}

void RtmpPush::on_btnSubmit_clicked()
{
    ui->textBrowser->append(" ");
    QString str;
    str.clear();
    if(ui->hlsBoth->isChecked()){
        str = (ui->isHttps->isChecked() ? "https://" : "http://") + push->getRtmpIp() + ":" + ui->HlsPort->text() +
                "/" + ui->HlsGroup->text() + "/" + push->getRtmpKey() + ".m3u8";
    }
    if(!ui->leHttpPostUrl->text().isEmpty())
    {
        bool ok = HttpRequest::getObject()->uploadHlsUrl(ui->leHttpPostUrl->text(),
                                               push->getPushUrl(),
                                               str, ui->id->text().toInt());
        if(ok){
            if(ui->hlsBoth->isChecked()){
                ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址POST提交成功"));
            }
            else
            {
                ui->textBrowser->append(QString::fromLocal8Bit("新RTMP/HLS地址POST提交成功"));
            }
            Configure::getObject()->setPostUrl(ui->leHttpPostUrl->text());
            Configure::getObject()->setPostID(ui->id->text().toInt());
        }
        else
            ui->textBrowser->append(QString::fromLocal8Bit("新地址提交失败，请检查地址并点击提交按钮"));
    }
    else {
        ui->textBrowser->append(QString::fromLocal8Bit("提交地址为空，放弃提交，可手动提交"));
    }
}

void RtmpPush::on_cbConnectType_currentIndexChanged(int index)
{
    switch (index) {
    case 0:
        Configure::getObject()->setRtspConnectType(false);
        push->setRtspConnectType(false);
        break;
    case 1:
        Configure::getObject()->setRtspConnectType(true);
        push->setRtspConnectType(true);
        break;
    }
}

void RtmpPush::prstop()
{
    push->stopPush();
    stat = false;
}

void RtmpPush::prstart()
{
    switch (ui->cbConnectType->currentIndex()) {
    case 0:
        Configure::getObject()->setRtspConnectType(false);
        push->setRtspConnectType(false);
        break;
    case 1:
        Configure::getObject()->setRtspConnectType(true);
        push->setRtspConnectType(true);
        break;
    }
    push->startPush(ui->rtsp->text().toStdString().c_str(),
                    ui->rtmp->text().toStdString().c_str());
}

void RtmpPush::on_btnSubmit_2_clicked()
{
    ui->textBrowser->append(" ");
    QString str;
    if(ui->hlsBoth->isChecked()){
        str = (ui->isHttps->isChecked() ? "https://" : "http://") + push->getRtmpIp() + ":" + ui->HlsPort->text() +
                "/" + ui->HlsGroup->text() + "/" + push->getRtmpKey() + ".m3u8";
    }
    else
        str = "http://empty.com";
    if(!ui->leHttpGetUrl->text().isEmpty())
    {
        bool ok = HttpRequest::getObject()->uploadHlsUrl2(ui->leHttpGetUrl->text(),
                                               str, ui->key->text());
        if(ok){
            if(ui->hlsBoth->isChecked()){
                ui->textBrowser->append(QString::fromLocal8Bit("新HLS地址GET提交成功"));
            }
            else
            {
                ui->textBrowser->append(QString::fromLocal8Bit("因未选择HLS功能，提交空地址，成功"));
            }
            Configure::getObject()->setGetUrl(ui->leHttpGetUrl->text());
            Configure::getObject()->setGetKey(ui->key->text());
        }
        else
            ui->textBrowser->append(QString::fromLocal8Bit("新HLS地址GET提交失败，请检查地址并点击提交按钮"));
    }
    else {
        ui->textBrowser->append(QString::fromLocal8Bit("提交地址为空，放弃提交，可手动提交"));
    }
}

void RtmpPush::on_hlsBoth_clicked(bool checked)
{
    if(checked)
    {
        ui->HlsGroup->setEnabled(true);
        ui->HlsPort->setEnabled(true);
        ui->isHttps->setEnabled(true);
        ui->leHttpGetUrl->setEnabled(true);
        ui->key->setEnabled(true);
        ui->btnSubmit_2->setEnabled(true);
    }
    else
    {
        ui->HlsGroup->setEnabled(false);
        ui->HlsPort->setEnabled(false);
        ui->isHttps->setEnabled(false);
        ui->leHttpGetUrl->setEnabled(false);
        ui->key->setEnabled(false);
        ui->btnSubmit_2->setEnabled(false);
    }
}

void RtmpPush::on_btnSubmit_3_clicked()
{
    ui->textBrowser->append(" ");
    QString str;
    str.clear();
    str = push->getPushUrl();
    if(!ui->leHttpGetUrl_2->text().isEmpty())
    {
        bool ok = HttpRequest::getObject()->uploadHlsUrl2(ui->leHttpGetUrl_2->text(),
                                               str, ui->key_2->text());
        if(ok){
            ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址GET提交成功"));
            Configure::getObject()->setGet2Url(ui->leHttpGetUrl_2->text());
            Configure::getObject()->setGet2Key(ui->key_2->text());
        }
        else
            ui->textBrowser->append(QString::fromLocal8Bit("新RTMP地址GET提交失败，请检查地址并点击提交按钮"));
    }
    else {
        ui->textBrowser->append(QString::fromLocal8Bit("提交地址为空，放弃提交，可手动提交"));
    }
}
